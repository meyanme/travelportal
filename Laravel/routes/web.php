<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/index', function () {
    return view('index');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/agents', function () {
    return view('agents');
});

Route::get('/blog', function () {
  return view('blog');
});

Route::get('/contact', function () {
  return view('contact');
});

Route::get('/property-single', function () {
  return view('property-single');
});

Route::get('/property', function () {
  return view('property');
});

Route::get('/single', function () {
  return view('single');
});
