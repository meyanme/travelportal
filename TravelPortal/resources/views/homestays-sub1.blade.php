<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,600,700" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">

    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">


    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://code.iconify.design/1/1.0.6/iconify.min.js"></script>

    <link rel="stylesheet" href="css/added-css.css">

    <!-- <script type="text/javascript" src="resources/js/amenitiespopup.js"></script> -->


  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light scrolled-awake scrolled awake" id="ftco-navbar">
      <div class="container">
        <a class="navbar-brand" href="index">FS Travel</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item"><a href="index" class="nav-link">Home</a></li>
            <li class="nav-item active"><a href="homestays" class="nav-link">Homestays</a></li>
            <li class="nav-item"><a href="experiences" class="nav-link">Experiences</a></li>
            <li class="nav-item"><a href="about" class="nav-link">About Us</a></li>
            <li class="nav-item"><a href="loginregister" class="nav-link">Login / Register</a></li>
          </ul>
          <a href="#" class="btn btn-primary">+ Add Listings</a>
        </div>
      </div>
    </nav>
    <!-- END nav -->

    <!-- <section class="home-slider owl-carousel">
      <div class="slider-item" style="" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
          <div class="row slider-text align-items-center">
            <div class="col-md-7 col-sm-12 ftco-animate">
              <p class="breadcrumbs"><span class="mr-2"><a href="index">Home</a></span> <span>Homestays</span></p>
              <h1 class="mb-3">Homestays</h1>
            </div>
          </div>


        </div>
      </div>
    </section> -->
    <!-- END slider -->

    <section class="ftco-section">
      <div class="container">
        <div class="ftco-animate">
            <!--NAME FROM DB-->
            <h1 class="heading">Eclipse@Pangea</h1>
            <div class="text">
              <span class="titleicon">
                <!--LOC FROM DB-->
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <a href="#">Cyberjaya, Selangor</a>

                <!--OWNER FROM DB-->
                <i class="fa fa-user" aria-hidden="true"></i>
                <a href="#">Flow Studios Sdn Bhd</a>
              </span>
            </div>
        </div>
        <!--END TITLE-->

        <div class="row">
          <div class="col-lg-8">

            <div class="row">
              <div class="col-md-6 col-lg-12 mb-4 ftco-animate">
                <a href="#" class="block-5" style="background-image: url('images/testimg/img4.jpeg');"></a>

              </div>
            </div>
              <!-- ------------------HOMESTAY DESCRIPTION SHORT------------------ -->
            <div class="row">
              <div class="text">
                <!--NAME FROM DB-->
                <h2 class="heading">Luxury Apartment in centre of Cyberjaya</h2>
                <!-- <p class="star-rate"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star-half-full"></span> <span>500 reviews</span></p> -->

                <h6> <!--REPLACE WITH VALUE FROM DB-->
                  <!--TYPE-->
                  <i class="fa fa-home pd_bot" aria-hidden="true"></i> Apartment __ sqft </br>
                  <!--PAX-->
                  <i class="fa fa-users pd_bot" aria-hidden="true"></i> 3 pax </br>
                  <!--BATHROOM-->
                  <i class="fa fa-bath pd_bot" aria-hidden="true"></i> 2 washrooms </br>
                  <!---->
                  <!-- <i class="fa fa-car pd_bot" aria-hidden="true"></i>2 parking lots </br> -->
                  <!-- <i class="fa fa-plus-square pd_bot" aria-hidden="true"></i> WiFi connectivity, Cooking Ammenities, Swimming Pool, Gymnasium, Sauna. -->
                </h6>

              </div>
            </div>
            <hr> <!-- ------------------HOMESTAY DESCRIPTION LONG------------------ -->
</br>

            <div class="row">
              <div class="text col ftco pd_topbot">
                <div class="text-inner align-self">
                  <h5 class="heading p_fx">Description</h5>
                  <address class="">
                    Adress: lorem ipsum
                  </address>

                  Lorem ipsum description bla bla
                </div>
              </div>
            </div>
</br>
            <hr> <!-- ------------------BED DESCRIPTION------------------ -->
</br>
            <div class="row">
              <label for="num">Sample input</label> </br>
              <input id="inputnum" type="number" name="num" placeholder="Enter any value >0">
              <button id="buttonnombo" name="submitbtn" onclick="fx()">OK</button>

            </div>
            <h5>Bedding Description</h5>
            <div id="sini" class="row">
              <div id ="clone" class="item text-center bed_padding hidden">      <!--buat content dalam viewer center-->
                <div class="testimony-wrap p-4 pb-5">
                  <h5 class="heading">
                    <i class="fa fa-bed pd_bot" aria-hidden="true"></i>
                    <!-- <p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p> -->
                    <p id="bedroomnum" class="text"> </p>
                  </h5>
                </div>
              </div>
            </div>
            <hr> <!-- ------------------AMENITIES DESCRIPTION------------------ -->
</br>
            <div class="row">
              <label for="num">Sample input</label> </br>
              <input id="inputtext" type="text" name="amenities" placeholder="Enter any amenities">
              <button id="amenities" name="submitbtn2" onclick="method()">OK</button>
            </div>

            <div class="row">
              <div class="text col ftco pd_topbot">
                <div class="text-inner align-self">
                  <h5 class="heading p_fx">Amenities</h5>
                  <table>
                    <tr id="parking" class="hidden">
                      <td><i class="fa fa-car" aria-hidden="true"></i></td>
                      <td> 2 parking lots </td>
                    </tr>
                    <tr id="wifi" class="hidden">
                      <td><i class="fa fa-wifi" aria-hidden="true"></i></td>
                      <td> Wifi </td>
                    </tr>
                    <tr id="kitchen" class="hidden">
                      <td><i class="fa fa-cutlery" aria-hidden="true"></i></td>
                      <td> Kitchen </td>
                    </tr>
                    <tr id="pool" class="hidden">
                      <td><i class="iconify" data-icon="fa-solid:swimming-pool" data-inline="false"></i></td>
                      <td> Pool </td>
                    </tr>
                    <tr id="iron" class="hidden">
                      <td><i class="iconify" data-icon="ps:iron-any-temp" data-inline="false"></i></td>
                      <td> Iron </td>
                    </tr>
                  </table>
</br>
                  <div class="popup" onclick="myFunction()"> See full amenities
                    <span class="popuptext" id="myPopup">
                      <ul>
                        <li>2 Parking Lots</li>
                        <li>Wifi Service</li>
                        <li>Kitchen Utensil</li>
                        <li>Swimming Pool</li>
                        <li>Iron</li>
                        <li>Satellite TV</li>
                      </ul>
                    </span>
                  </div>

                  <!-- <script>
                  // When the user clicks on div, open the popup
                    function myFunction() {
                      var popup = document.getElementById("myPopup");
                      popup.classList.toggle("show");
                    }
                  </script> -->


                </div>
              </div>
            </div>
</br>
            <hr> <!-- ------------------CUSTOMERREVIEW------------------ -->
</br>

            <!-- <div class="row mt-5">
              <div class="col text-center">
                <div class="block-27">
                  <ul>
                    <li><a href="#">&lt;</a></li>
                    <li class="active"><span>1</span></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">&gt;</a></li>
                  </ul>
                </div>
              </div>
            </div> -->

            <div class="row">
              <div class="sidebar-box ftco bg-light">
                <h2 id="counter" class="heading">
                  Customer Review (<script> countRvw; </script>)
                </h2> <!--num of reviews-->

                <div id="reviewsection">
                  <div id="review" class="container_review">
                    <img src="images/person_2.jpg" alt="Avatar" style="width:90px">
                    <p><span>Huda Shahira</span><!-- CEO at Mighty Schools. --></p>
                    <p>Good!.</p>
                  </div>

                  <div id="review" class="container_review">
                    <img src="images/person_1.jpg" alt="Avatar" style="width:90px">
                    <p><span >Ahmad Syamil.</span> <!--CEO at Company.--></p>
                    <p>Great!.</p>
                  </div>

                  <script>
                    $(document).ready(function(){
                      var divCount = $("#review").length;
                      alert(divCount);
                    });
                  </script>

                </div>
<br>
                <form action="" method="">
                  <div class="fields">
                    <div class="row flex-column">
                      <div class="check-in col-sm-12 group mb-3"><input type="text" id="new_review" class="form-control" placeholder="Enter your review"></div>
                      <div class="col-sm-12 group mb-3">
                        <input type="submit" class="search-submit btn btn-primary" value="Submit review">
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <!-- END -->

            <div class="col-lg-4 sidebar">
              <div class="sticky">
                <div class="sidebar-box ftco-animate">
                  <div class="search-tours bg-light p-4">
                    <h3>RM ###/night</h3>       <!--REPLACE WITH PRICE FROM DB-->
                    <form action="" method="post">
                      <div class="fields">
                        <div class="row flex-column">

                          <!-- <div class="textfield-search col-sm-12 group mb-3"><input type="text" class="form-control" placeholder="Search Location"></div> -->

                          <div class="check-in col-sm-12 group mb-3"><input type="text" id="checkin_date" class="form-control" placeholder="Check-in date"></div>

                          <div class="check-out col-sm-12 group mb-3"><input type="text" id="checkout_date" class="form-control" placeholder="Check-out date"></div>
                          <div class="select-wrap col-sm-12 group mb-3">
                            <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                            <select name="" id="" class="form-control">
                              <option value="">Guest</option>
                              <option value="">1</option>
                              <option value="">2</option>
                              <option value="">3</option>
                              <option value="">4+</option>
                            </select>
                          </div>
                          <div class="col-sm-12 group mb-3">
                            <input type="submit" class="search-submit btn btn-primary" value="Reserve Now">
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>

                <!-- <div class="sidebar-box ftco-animate">
                  <div class="categories">
                    <h3>Categories</h3>
                    <li><a href="#">Tours <span>(12)</span></a></li>
                    <li><a href="#">Hotels <span>(22)</span></a></li>
                    <li><a href="#">Cruises <span>(37)</span></a></li>
                    <li><a href="#">Restaurant <span>(42)</span></a></li>
                    <li><a href="#">Destination <span>(14)</span></a></li>
                  </div>
                </div>

                <div class="sidebar-box ftco-animate">
                  <h3>Tag Cloud</h3>
                  <div class="tagcloud">
                    <a href="#" class="tag-cloud-link">Life</a>
                    <a href="#" class="tag-cloud-link">Sport</a>
                    <a href="#" class="tag-cloud-link">Tech</a>
                    <a href="#" class="tag-cloud-link">Travel</a>
                    <a href="#" class="tag-cloud-link">Life</a>
                    <a href="#" class="tag-cloud-link">Sport</a>
                    <a href="#" class="tag-cloud-link">Tech</a>
                    <a href="#" class="tag-cloud-link">Travel</a>
                  </div>
                </div> -->


            </div>

        </div>
      </div>


<!--#################################################-->
      <!-- <div class="slider-container">

        <div class="hotelSlides fade">
          <div class="numbertext">1 / 2</div>
          <a href="#" class="block-5" style="background-image: url('images/hotel-1.jpg');">
          <img src="hotel-1.jpg" style="width: 100%">
        </div>

        <div class="hotelSlides fade">
          <div class="numbertext">2 / 2</div>
          <a href="#" class="block-5" style="background-image: url('images/hotel-5.jpg');">
          <img src="images/hotel-5.jpg" style="width: 100%">
        </div>

        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>

      </div>
      <br>
      <div style="text-align:center">
        <span class="dot" onclick="currentSlide(1)"></span>
        <span class="dot" onclick="currentSlide(2)"></span>
      </div> -->

    </section>

    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Voyage Fellow Tourist</h2>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Book Now</h2>
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block">Flight</a></li>
                <li><a href="#" class="py-2 d-block">Hotels</a></li>
                <li><a href="#" class="py-2 d-block">Tour</a></li>
                <li><a href="#" class="py-2 d-block">Car Rent</a></li>
                <li><a href="#" class="py-2 d-block">Beach &amp; Resorts</a></li>
                <li><a href="#" class="py-2 d-block">Mountains</a></li>
                <li><a href="#" class="py-2 d-block">Cruises</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Top Deals</h2>
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block">Luxe Hotel</a></li>
                <li><a href="#" class="py-2 d-block">Venice Tours</a></li>
                <li><a href="#" class="py-2 d-block">Deluxe Hotels</a></li>
                <li><a href="#" class="py-2 d-block">Boracay Beach &amp; Resorts</a></li>
                <li><a href="#" class="py-2 d-block">Beach &amp; Resorts</a></li>
                <li><a href="#" class="py-2 d-block">Fuente Villa</a></li>
                <li><a href="#" class="py-2 d-block">Japan Tours</a></li>
                <li><a href="#" class="py-2 d-block">Philippines Tours</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Contact Information</h2>
              <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block">198 West 21th Street, Suite 721 New York NY 10016</a></li>
                <li><a href="#" class="py-2 d-block">+ 1235 2355 98</a></li>
                <li><a href="#" class="py-2 d-block">info@yoursite.com</a></li>
                <li><a href="#" class="py-2 d-block">email@email.com</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <ul class="ftco-footer-social list-unstyled float-md-right float-lft">
                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>



  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
  <script src="js/addedjs.js"></script>
  </body>
</html>


<!-- <div class="row">
  <div class="col-md-6 col-lg-12 mb-4 ftco-animate">
    <!
    <div class="container_slider">
      <div class="slidercontent">
        <a href="#" class="block-5" style="background-image: url('images/testimg/img4.jpeg');"></a>
      </div>
      <div class="slidercontent">
        <a href="#" class="block-5" style="background-image: url('images/testimg/img3.jpeg');"></a>
      </div>
      <div class="slidercontent">
        <a href="#" class="block-5" style="background-image: url('images/testimg/img5.jpeg');"></a>
      </div>
      <div class="slidercontent">
        <a href="#" class="block-5" style="background-image: url('images/testimg/img8.jpeg');"></a>
      </div>

      <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
      <a class="next" onclick="plusSlides(1)">&#10095;</a>

      <div class="row_thumbnail">
        <div class="column_thumbnail">
          <img class="opacity cursor style" src="images/testimg/img4.jpeg" alt="img4" style="width: 100%;" onclick="currentSlide(1)">
        </div>
        <div class="column_thumbnail">
          <img class="opacity cursor style" src="images/testimg/img3.jpeg" alt="img3" style="width: 100%;" onclick="currentSlide(2)">
        </div>
        <div class="column_thumbnail">
          <img class="opacity cursor style" src="images/testimg/img5.jpeg" alt="img5" style="width: 100%;" onclick="currentSlide(5)">
        </div>
        <div class="column_thumbnail">
          <img class="opacity cursor style" src="images/testimg/img8.jpeg" alt="img8" style="width: 100%;" onclick="currentSlide(4)">
        </div>
      </div>
    </div>

  </div>
</div> -->
