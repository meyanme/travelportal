<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/index', function () {
    return view('index');
});

Route::get('/homestays', function () {
    return view('homestays');
});

Route::get('/homestays-sub1', function() {
    return view('homestays-sub1');
});

Route::get('/experiences', function () {
    return view('experiences');
});

Route::get('/experiences-sub1', function() {
    return view('experiences-sub1');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/loginregister', function () {
    return view('loginregister');
});

Route::get('/about', function () {
    return view('about');
});


Route::get('/blog-single', function () {
    return view('blog-single');
});

Route::get('/blog', function () {
    return view('blog');
});

Route::get('/prepos-6', function () {
    return view('prepos-6');
});

Route::get('/tours', function () {
    return view('tours');
});
